# Vivliostyle Electron PDF Renderer

Render Html to PDFs using [Vivliostyle.js](https://github.com/vivliostyle/vivliostyle.js) and [Electron](https://github.com/bjrmatos/electron-html-to).

## Installation

```
npm install -g vivliostyle-electron
```

## Generating a PDF

```
vivliostyle-electron ./path/to/index.html -o result.pdf
```

## Options

```
-h, --help                  output usage information
-V, --version               output the version number
-i, --inputs [inputs]       Inputs
-o, --output [output]       Output
-d, --debug                 Show Electron Window to Debug
-l, --landscape             Landscape printing
-s, --page-size [size]      Print to Page Size [size]
-w, --width [size]          Print to Page Width [width]
-h --height [size]          Print to Page Height [weight]
-m, --page-margin [margin]  Print with margin [margin]
-n, --hyphenate [lang]      Hyphenate with language [language], defaults to "en-us"
-hi, --hypher_ignore [str]  Ignore passed element selectors, such as ".class_to_ignore, h1"
-ho, --hypher_only [str]    Only hyphenate passed elements selector, such as ".hyphenate, aside"
-e, --encoding [type]       Set the encoding of the input html, defaults to "utf-8"
-t, --timeout [ms]          Set a max timeout of [ms]
```

## Hyphenation

HTML can be pre-processed with soft hyphens by the [Hypher](https://github.com/bramstein/hypher) library.

Pass the abbreviation a language code (such as `en-us` or `de`) when calling the renderer. You can install languages beyond those included the package.json using npm.


```
vivliostyle-electron ./path/to/index.html --hyphenate en-us --output
```

## Running on Linux

Install Xvfb
```
sudo apt-get install libgtk2.0-dev libxtst-dev libxss1 libgconf2-dev libnss3-dev libasound2-dev xvfb
```

Run Xvfb
```
export DISPLAY=':99.0'
Xvfb :99 -screen 0 1024x768x24 > /dev/null 2>&1 &
```

More details here: https://github.com/bjrmatos/electron-html-to/issues/87

## Development
Link and build the JS
```
npm install
npm link
npm install -g gulp

gulp watch
```

Then you can run the command with `DEBUG` flags for just this library
```
DEBUG=vivliostyle-electron:* vivliostyle-electron ./path/to/index.html --output
```

or for all conversion processes
```
DEBUG=* vivliostyle-electron ./path/to/index.html --output
```

To display the output in the browser window before printing,
instead of outputting the file add the `--debug` flag.

```
vivliostyle-electron ./path/to/index.html --debug
```

## Testing

Install Mocha with `npm install -g mocha`

Run the tests from the library root with the `mocha` command
```
mocha
```

## Docker
```bash
docker run -it -P -v $(pwd):/usr/src/vivliostyle-electron fchasen/vivliostyle-electron bash
```
