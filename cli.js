#!/usr/bin/env node

var Renderer = require("./lib/index.js");
var Hyphenator = require("./lib/hyphenator.js");
var program = require('commander');
var path = require('path');
var fs = require('fs');
var replaceExt = require('replace-ext');
var temp = require("temp").track();


program
  .version(require('./package.json').version)
  .arguments('[inputPath]')
  .option('-i, --inputs [inputs]', 'Inputs')
  .option('-o, --output [output]', 'Output')
  .option('-d, --debug', 'Debug')
  .option('-l, --landscape', 'Landscape printing', false)
  .option('-s, --page-size [size]', 'Print to Page Size [size]')
  .option('-w, --width [size]', 'Print to Page Width [width] in MM')
  .option('-h --height [size]', 'Print to Page Height [weight] in MM')
  .option('-m, --page-margin [margin]', 'Print with margin [margin]')
  .option('-n, --hyphenate [lang]', 'Hyphenate with language [language], defaults to "en-us"')
  .option('-hi, --hypher_ignore [str]', 'Ignore passed element selectors, such as ".class_to_ignore, h1"')
  .option('-ho, --hypher_only [str]', 'Only hyphenate passed elements selector, such as ".hyphenate, aside"')
  .option('-e, --encoding [type]', 'Set the encoding of the input html, defaults to "utf-8"')
  .option('-t, --timeout [ms]', 'Set a max timeout of [ms]')
  .parse(process.argv);

var input = program.inputs || program.args[0];

if (!input) {
  console.error("You must include an input path");
  return process.exit(1);
}

if (program.debug) {
  process.env.ELECTRON_HTML_TO_DEBUGGING = true;
}

var renderer = new Renderer(program);

var dir = process.cwd();
var relativePath = path.resolve(dir, input);
var output;
var tmpFile, tmpPath;

var hyphenator;
var hyphenateOptions;


if (['.html', '.xhtml'].indexOf(path.extname(relativePath)) === -1) {
  console.error("Must pass a html or xhtml file as input");
  return process.exit(1);
}

try {
    fs.accessSync(relativePath, fs.F_OK);
} catch (e) {
    console.error("Input cannot be found", e);
    return process.exit(1);
}

if (typeof(program.output) === "string") {
  output = path.resolve(dir, program.output);
} else if (typeof(program.output) != "undefined") {
  output = './' + replaceExt(path.basename(input), '.pdf');
}


if (program.hyphenate) {
  hyphenateOptions = {
    ignore: program.hypher_ignore || undefined,
    only: program.hypher_only || undefined,
    encoding: program.encoding || undefined
  }

  tmpPath = replaceExt(relativePath, ".hyphenated.html");

  // tmpFile = temp.openSync({suffix: '.html'});
  // tmpPath = tmpFile.path;
  // Create a new Hyphenator, with passed language
  hyphenator = new Hyphenator(program.hyphenate);
  hyphenator.process(relativePath, tmpPath, hyphenateOptions);

  console.log("Hyphenated for", typeof(program.hyphenate) === "string" ? program.hyphenate : "en-us");

  if (program.debug && tmpPath) {
    console.log("Hyphenated file at:", tmpPath);
  }

}

renderer.process(tmpPath || input, output)
  .then(function (result) {
    var msg = "Printed";

    if (result) {
      msg += " " + result.numberOfPages + " pages";
    }

    if (output) {
      msg += " to  " + output;
    }

    console.log(msg);

    if (tmpPath && !program.debug) {
      fs.unlinkSync(tmpPath);
    }

  }).catch(function (err) {
    // temp.cleanupSync();
    console.error(err);
  });
