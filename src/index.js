import convertFactory from 'electron-html-to';
import fs from 'fs';
import path from 'path';
import debug from 'debug';
import { name as pkgName } from '../package.json';
import mkdirp from 'mkdirp';

const log = debug(`${pkgName}:renderer`);

const MM_TO_MICRON = 1000;

function Renderer(options) {

  this.settings = this.determineSettings(options);

  this.converter = convertFactory({
    converterPath: convertFactory.converters.PDF,
    allowLocalFilesAccess: true,
    timeout: this.settings.timeout, // Allow five minutes to process
    strategy: this.settings.strategy // 'electron-ipc | electron-server | dedicated-process'
  });

}

Renderer.prototype.determineSettings = function (_options) {
  var options = _options || {};

  return {
    pageSize: options.pageSize || 'A4',
    pageMargin: options.pageMargin || 0,
    landscape: options.landscape || false,
    width: options.width,
    height: options.height,
    debug: options.debug || false,
    timeout: options.timeout || 60000 * 5,
    strategy: options.strategy || 'dedicated-process',
    keepAlive: options.keepAlive || false
  };
};

Renderer.prototype.process = function (inputPath, outputPath, options) {
  var settings = options ? this.determineSettings(options) : this.settings;
  var exists = this.fileExists(inputPath);
  var result;

  if (!exists) {
    log("Input cannot be found: %s", inputPath);
    if (settings.keepAlive != true) {
      this.converter.kill();
    }
    return new Promise(function(resolve, reject) {
      reject(err);
    });
  }

  log("Processing from input %s", inputPath);

  result = this.convert(inputPath, settings);

  result.then((result) => {

      log("Printed %s pages to %s", result.numberOfPages, outputPath);

      if (settings.keepAlive != true) {
        this.converter.kill();
      }

      if (outputPath) {
        this.output(result.stream, outputPath);
      }

    }, (err) => {

      log('An error has ocurred in in converting [%s]: %s',  inputPath, err.message, err.stack);

      if (settings.keepAlive != true) {
        this.converter.kill();
      }

    });

  return result;
};

Renderer.prototype.fileExists = function (inputPath) {
  try {
    fs.accessSync(inputPath, fs.F_OK);
    return true;
  } catch (err) {
    return false;
  }
};

Renderer.prototype.getRendererPath = function () {
  var directoryPath =  path.normalize(__dirname);
  var rendererPath = path.resolve(directoryPath, '../renderer');

  if (rendererPath[rendererPath.length] != "/") {
    rendererPath += "/";
  }

  return rendererPath;
};

Renderer.prototype.getRelativePath = function (basePath, inputPath) {
  var relativePath = path.relative(basePath, inputPath);
  return relativePath;
};


Renderer.prototype.encodeUrl = function (inputPath) {
  var rendererPath =  this.getRendererPath();

  var relativePath = this.getRelativePath(rendererPath, inputPath);

  var url = 'file://' + rendererPath + "renderer.html?url=" + encodeURIComponent(relativePath);

  return url;
}

Renderer.prototype.settingsToUriComponent = function (settings) {
  var url = '';

  // ignore page size if width & height are present
  if (settings.width && settings.height) {
    url += '&width=' + settings.width;
    url += '&height=' + settings.height;
  } else if (settings.pageSize) {
    url += '&pageSize=' + settings.pageSize;
  }

  if (settings.landscape) {
    url += '&landscape=' + settings.landscape;
  }

  if (settings.debug) {
    url += '&debug=' + settings.debug;
  }

  return url;
};

Renderer.prototype.getSize = function(settings) {
  var size = settings.pageSize;

  if (settings.width && settings.height) {
    size = {
      width: parseFloat(settings.width) * MM_TO_MICRON,
      height: parseFloat(settings.height) * MM_TO_MICRON
    };
  }

  return size;
}

Renderer.prototype.convert = function(inputPath, settings) {
  var url = this.encodeUrl(inputPath, settings);
  var fullUrl = url + this.settingsToUriComponent(settings);

  log("Converting from %s", fullUrl);

  return new Promise(function(resolve, reject) {
    this.converter({
        url: fullUrl,
        waitForJS: true,
        pdf: {
          marginsType: settings.pageMargin,
          pageSize: this.getSize(settings),
          printBackground: true,
          landscape: settings.landscape
        }
      }, function(err, result) {

      if (err) {
        return reject(err);
      }

      resolve(result);

    });

  }.bind(this));
}

Renderer.prototype.output = function (stream, outputPath) {
  mkdirp.sync(path.dirname(outputPath));
  stream.pipe(fs.createWriteStream(outputPath));
};

export default Renderer;
