var viewer;
var viewerSettings = {
    userAgentRootURL: "resources/",
    viewportElement: document.getElementById("vivliostyle-viewer-viewport"),
    debug: true
};
var viewerOptions = {
    fontSize: 16,
    profile: false,
    spreadView: false,
    zoom: 1
};
var documentOptions = {
  userStyleSheet:[{ text: "@page { size: A4; }" }]
};

var search = window.location.search;
var settings = {};
var parameters;

// Overide options with search parameters
if(search) {
  parameters = search.slice(1).split("&");
  parameters.forEach(function(p){
    var split = p.split("=");
    var name = split[0];
    var value = split[1] || '';
    var parsedValue = decodeURIComponent(value);

    if (parsedValue === "true") {
      parsedValue = true;
    }

    if (parsedValue === "false") {
      parsedValue = false;
    }

    settings[name] = parsedValue;
  });
}

if (settings.debug) {
  viewerSettings.debug = settings.debug;
}

viewer = new vivliostyle.viewer.Viewer(viewerSettings, viewerOptions);

viewer.addListener("loaded", function() {
    window.ELECTRON_HTML_TO_READY = true; // this will start the conversion
});

if (settings.width && settings.height) {
  documentOptions.userStyleSheet[0].text = "@page { size: "+settings.width+" "+settings.height+"; }";
} else if (settings.pageSize && settings.landscape) {
  documentOptions.userStyleSheet[0].text = "@page { size: "+settings.pageSize+" landscape; }";
} else if (settings.pageSize) {
  documentOptions.userStyleSheet[0].text = "@page { size: "+settings.pageSize+"; }";
}

viewer.loadDocument(settings.url, documentOptions, viewerOptions);
