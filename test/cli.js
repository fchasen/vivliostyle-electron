var assert = require('assert');
var Renderer = require("../lib/index.js");

var spawn = require('child_process').spawn;
var path = __dirname.replace("test", '') + "cli.js";
var fs = require('fs');

describe('CLI', function() {

  this.timeout(5000);

  it('should not error when a html file is passed', function(done) {

    var cli = spawn(path, ["./test/samples/wood/index.html"])

    cli.on('close', function(code){
      assert.equal(code, 0);
      done();
    });

    cli.on('error', function(code){
      console.log("error");
      done();
    });

  });

  it('should not error when a xhtml file is passed', function(done) {

    var cli = spawn(path, ["./test/samples/TheMalayArchipelago/main.xhtml"])

    cli.on('close', function(code){
      assert.equal(code, 0);
      done();
    });

  });

  it('should error when no input is passed', function(done) {

    var cli = spawn(path, [""]);

    cli.on('close', function(code){
      assert.equal(code, 1);
      done();
    });

  });

  it('should error when non-html input is passed', function(done) {

    var cli = spawn(path, ["/this/is/a.pdf"]);

    cli.on('close', function(code){
      assert.equal(code, 1);
      done();
    });

  });

  it('should error when a non exisiting input is passed', function(done) {

    var cli = spawn(path, ["/this/is/bad/file.html"]);

    cli.on('close', function(code){
      assert.equal(code, 1);
      done();
    });

  });

  it('should report correct number of pages', function(done) {
    var cli = spawn(path, ["./test/samples/wood/index.html"])

    cli.stdout.on('data', function(data) {
      var dataStr = data.toString();
      assert.equal(dataStr, "Printed 5 pages\n");
      done();
    });
  });

  it('should output to a given path', function(done) {

    var cli = spawn(path, ["./test/samples/wood/index.html", "--output", "./test/output/wood.pdf"])
    var exists = false;

    cli.on('close', function(code){

      fs.access("./test/output/wood.pdf", fs.F_OK, function(err) {
        if (err) {
          exists = false;
        } else {
          exists = true;
        }

        assert.equal(exists, true);
        done();
      });

    });

  });

  it('should Hyphenate when giving the --hyphenate option', function(done) {

    var cli = spawn(path, ["./test/samples/wood/index.html", "--hyphenate"])
    var dataStr = "";
    cli.stdout.on('data', function(data) {
      dataStr += data.toString();
    });

    cli.on('close', function(code){

      assert(dataStr.indexOf("Hyphenated for en-us\n") > -1, "Was Hyphenated");

      assert.equal(code, 0);
      done();
    });

  });

});
