var assert = require('assert');
var Renderer = require("../lib/index.js");
var fs = require('fs');

describe('Renderer', function() {

  describe('#init', function() {
    var renderer = new Renderer();
    it('should create a convertFactory', function() {
      assert.notEqual(renderer.converter, undefined);
    });
  });

  describe('#determineSettings', function() {
    var renderer = new Renderer();

    it('should return defaults', function() {
      var settings = renderer.determineSettings();

      assert.deepEqual(settings, {
        pageSize: 'A4',
        pageMargin: 0,
        landscape: false,
        width: undefined,
        height: undefined,
        debug: false,
        timeout: 60000 * 5,
        strategy: 'dedicated-process',
        keepAlive: false
      });

    });

    it('should overrided defaults', function() {
      var options = {
        pageSize: 'letter',
        pageMargin: 10,
        landscape: true,
        width: 100,
        height: 200,
        debug: true,
        timeout: 1,
        strategy: 'electron-ipc',
        keepAlive: true
      };

      var settings = renderer.determineSettings(options);

      assert.deepEqual(settings, options);

    });

  });

  describe('#getRendererPath', function() {
    var renderer = new Renderer();

    it('should return the path to the renderer html', function() {
      var path = renderer.getRendererPath();
      assert.equal(path, __dirname.replace("test", "renderer/"));
    });

  });

  describe('#getRelativePath', function() {
    var renderer = new Renderer();

    it('should resolve with a relative path', function() {
      var path = renderer.getRelativePath(__dirname, "./test/x/y/z/test.html");
      assert.equal(path, "x/y/z/test.html");
    });

    it('should resolve with a relative path up a lever', function() {
      var path = renderer.getRelativePath(__dirname, "../test.html");
      assert.equal(path, "../../test.html");
    });

  });


  describe('#encodeUrl', function() {
    var renderer = new Renderer();

    it('should encode a url', function() {
      var inputPath = "./test/samples/wood/index.html";
      var path = renderer.encodeUrl(inputPath);
      assert.equal(path, "file://" + __dirname.replace("test", "renderer") + "/renderer.html?url=..%2Ftest%2Fsamples%2Fwood%2Findex.html");
    });

  });

  describe('#settingsToUriComponent', function() {
    var renderer = new Renderer();

    it('should allow for empty settings', function() {

      var component = renderer.settingsToUriComponent({});
      assert.equal(component, "");
    });

    it('should turn width & height into a url component', function() {
      var settings = {
        width: 100,
        height: 600
      };
      var component = renderer.settingsToUriComponent(settings);
      assert.equal(component, "&width=100&height=600");
    });

    it('should set pageSize', function() {
      var settings = {
        pageSize: "letter"
      };
      var component = renderer.settingsToUriComponent(settings);
      assert.equal(component, "&pageSize=letter");
    });

    it('should set landscape', function() {
      var settings = {
        landscape: true
      };
      var component = renderer.settingsToUriComponent(settings);
      assert.equal(component, "&landscape=true");
    });

    it('should set debug', function() {
      var settings = {
        debug: true
      };
      var component = renderer.settingsToUriComponent(settings);
      assert.equal(component, "&debug=true");
    });

    it('should ignore page size if width & height are present', function() {
      var settings = {
        width: 100,
        height: 600,
        pageSize: "letter",
        landscape: true,
        debug: true
      };
      var component = renderer.settingsToUriComponent(settings);
      assert.equal(component, "&width=100&height=600&landscape=true&debug=true");
    });

  });

  describe('#getSize', function() {
    var renderer = new Renderer();

    it('should return width and height in microns from settings', function() {
      var settings = {
        width: 100,
        height: 600,
        pageSize: "letter"
      };

      var component = renderer.getSize(settings);
      assert.deepEqual(component, { width: 100000, height: 600000});
    });

    it('should return pageSize from settings if no width and height', function() {
      var settings = {
        pageSize: "letter"
      };

      var component = renderer.getSize(settings);
      assert.equal(component, "letter");
    });

  });

  describe('#fileExists', function() {
    var renderer = new Renderer();

    it('should find a valid file', function() {
      var inputPath = "./test/samples/wood/index.html";
      var exists = renderer.fileExists(inputPath);
      assert(exists, "File was found");
    });

    it('should not find an invalid file', function() {
      var inputPath = "./test/samples/wood/fake.html";
      var exists = renderer.fileExists(inputPath);
      assert(exists === false, "File was not found");
    });

  });

  describe('#convert', function() {
    this.timeout(15000);

    var renderer = new Renderer();

    it('should return a result with stream & page numberOfPages', function() {
      var inputPath = "./test/samples/wood/index.html";
      return renderer.convert(inputPath, {})
        .then(function(result){
          assert(result != undefined, "Result should exist.");
          assert.equal(result.numberOfPages, 5);
          assert(result.stream != undefined, "Stream should exist.");
        });
    });

  });

  describe('#process', function() {
    this.timeout(15000);

    var renderer = new Renderer({keepAlive: true});

    it('should process a valid inputPath', function() {
      var inputPath = "./test/samples/wood/index.html";
      return renderer.process(inputPath, null)
        .then(function(result){
          assert(result != undefined, "Result should exist.");
          assert.equal(result.numberOfPages, 5);
          assert(result.stream != undefined, "Stream should exist.");
        });
    });

    it('should reject an invalid inputPath', function() {
      var inputPath = "./test/samples/wood/fake.html";
      return renderer.process(inputPath, null)
        .then(function(result){
          assert(false, "Should not return successfully");
        }, function(err) {
          assert(err != null, "Should return error");
        });

    });

    it('should output to the provided path', function(done) {
      var inputPath = "./test/samples/wood/index.html";
      var outputPath = "./test/output/process_test.pdf";

      renderer.process(inputPath, outputPath)
        .then(function(result){

          fs.access(outputPath, fs.F_OK, function(err) {
            var exists = false;
            if (err) {
              exists = false;
            } else {
              exists = true;
            }

            assert(exists, "File exists");
            done();
          });

        }).catch(function(err) {
          console.error(err);
          assert.equal(exists, undefined);
          done();
        });
    });

  });

});
