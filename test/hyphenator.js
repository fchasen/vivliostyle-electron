var assert = require('assert');
var Hyphenator = require("../lib/hyphenator.js");

var XHTML = '\
<!DOCTYPE html>\
<html lang="en">\
<head>\
<meta charset="UTF-8"/>\
<title>Wood Engraving</title>\
</head>\
<body>\
<h1 class="title">Wood Engraving</h1>\
<p class="author">R.J. Beedham</p>\
<h2>INTRODUCTION</h2>\
<p class="dropcap"><span class="dropcap">T</span>his book is not a treatise upon the art of wood-engraving. It is simply a description of the tools and materials required by a beginner and the method of using them. It is not intended to assist anyone to become a commercial engraver, for that trade requires a long and specialized training. It is intended rather for those who have occasion or opportunity or inclination to make illustrations or ornaments for books and who are revolted by the degradation to which the art of formal drawing has been brought by photographic &lsquo;process&rsquo; reproduction.</p>\
</body>\
</html>\
';


describe('Hyphenator', function() {

  describe('#init', function() {
    it('should load hyper', function() {
      var hyphenator = new Hyphenator();
      assert.notEqual(hyphenator.h, undefined);
    });

    it('should default to English', function() {
      var hyphenator = new Hyphenator();
    });

    it('should load language based on short-hand', function() {
      var hyphenator = new Hyphenator("nl");
      assert.equal(hyphenator.lang, "nl");
    });
  });

  describe('#process', function() {
    var hyphenator = new Hyphenator();

    it('should process a valid inputPath', function() {
      var inputPath = "./test/samples/wood/index.html";
      var result = hyphenator.process(inputPath);
      var hyphens = result.indexOf("\u00AD");

      assert.equal(result.length, 8729);

      // soft hyphens
      assert(hyphens > -1, "Hypens have been added" );
    });

    it('should reject an invalid inputPath', function() {
      var inputPath = "./test/samples/wood/fake.html";

      try {
        var result = hyphenator.process(inputPath);
      } catch (e) {
        assert(e, "Should throw an error");
      }

    });

  });

  describe('#toDocument', function() {
    var hyphenator = new Hyphenator();

    it('should load xhtml and return it unchanged', function() {
      var $doc = hyphenator.toDocument(XHTML);



      assert.equal($doc('html').length, 1);
    });

  });

  describe('#walk', function() {
    var hyphenator = new Hyphenator();

    it('should load xhtml and return it unchanged', function() {
      var $doc = hyphenator.toDocument(XHTML);

      var result = hyphenator.walk($doc, function(data){
        return data;
      }, {});

      assert.equal(result, XHTML);
    });

  });

});
