'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _cheerio = require('cheerio');

var _cheerio2 = _interopRequireDefault(_cheerio);

var _hypher = require('hypher');

var _hypher2 = _interopRequireDefault(_hypher);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var IGNORE = 'head, code, pre, script, style, [class^="pull-"], [class^="push-"], .small-caps';

function Hyphenator(_lang) {
  var lang;
  if (typeof _lang === "string") {
    lang = _lang;
  } else {
    lang = "en-us";
  }
  this.lang = lang;
  this.pattern = require("hyphenation." + this.lang);
  this.h = new _hypher2.default(this.pattern);
}

Hyphenator.prototype.hyphenateText = function (text) {
  var _this = this;

  // split the text in html entity and not entity
  return text.split(/(!?&[a-zA-Z]*;)/).map(function (textPart) {
    // immediately return html entities and hyphenate everything else
    return textPart.match(/&[a-zA-Z]*;/) ? textPart : _this.h.hyphenateText(textPart);
  }).join('');
};

Hyphenator.prototype.process = function (input, output, encoding, options) {
  var contents = _fs2.default.readFileSync(input, options && options.encoding || 'utf8');
  var $doc = this.toDocument(contents);
  var hyphenated;

  // this.baseTag($doc, input, output);

  hyphenated = this.walk($doc, this.hyphenateText.bind(this), options);

  if (output) {
    this.output(hyphenated, output, encoding);
  }

  return hyphenated;
};

Hyphenator.prototype.output = function (contents, outputPath, encoding) {
  _fs2.default.writeFileSync(outputPath, contents, encoding || 'utf8', encoding);
};

Hyphenator.prototype.findTextNodes = function ($, node, doThis, ignore) {
  var hyphenator = this;

  if ($(node).is(ignore)) return false;

  $(node).contents().each(function () {

    var childNode = $(this)[0];

    // We've made it to a text node!
    // apply the function which transforms
    // its text content (childNode.data)
    if (childNode.type === 'text' || !childNode.type) {
      childNode.data = doThis(childNode.data);
    } else {
      hyphenator.findTextNodes.call(hyphenator, $, this, doThis, ignore);
    }
  });
};

Hyphenator.prototype.toDocument = function (html) {

  var $ = _cheerio2.default.load(html, {
    decodeEntities: false,
    xmlMode: true
  });

  return $;
};

Hyphenator.prototype.baseTag = function ($, inputPath, outputPath) {
  var $base = $('base');
  var dir, resolved;
  if (!outputPath) {
    return;
  }

  dir = _path2.default.dirname(inputPath);
  resolved = _path2.default.resolve(dir);
  console.log("resolved", dir, resolved);

  // Add Base Tag
  if ($base.length === 0) {
    $base = $('<base/>');
    $('head').append($base);
  }

  $base.attr("href", "file://" + resolved + "/");
};

Hyphenator.prototype.walk = function ($, doThis, options) {
  var ignore = IGNORE;
  var only = options && options.only || ':root';
  var hyphenator = this;
  var processedText;

  if (options && options.ignore) ignore += ', ' + options.ignore;

  processedText = $(only).each(function () {
    hyphenator.findTextNodes.call(hyphenator, $, this, doThis, ignore);
  });

  return $.html({
    decodeEntities: false,
    xmlMode: true
  });
};

exports.default = Hyphenator;
module.exports = exports['default'];