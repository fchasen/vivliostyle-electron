FROM node:latest

ENV DEPLOY_TO=/usr/src/vivliostyle-electron

RUN apt-get update && apt-get install -y \
  libgtk2.0-dev libxtst-dev libxss1 libgconf2-dev libnss3-dev libasound2-dev \
  xvfb

# Create app directory
RUN mkdir -p $DEPLOY_TO
WORKDIR $DEPLOY_TO

# Install app dependencies
RUN npm install -g gulp mocha

COPY package.json $DEPLOY_TO/
RUN npm install

COPY . $DEPLOY_TO

RUN npm link

CMD ["/bin/bash","script/test"]
